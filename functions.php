
<?php

/* Theme setup */
add_action( 'after_setup_theme', 'wpt_setup' );
if ( ! function_exists( 'wpt_setup' ) ):
    function wpt_setup() {
        register_nav_menu( 'primary', __( 'Primary navigation', 'wptuts' ) );
    } endif;
function wpt_register_js() {
    wp_register_script('jquery.bootstrap.min', get_template_directory_uri() . '/js/bootstrap.min.js', 'jquery');
    wp_enqueue_script('jquery.bootstrap.min');
}
add_action( 'init', 'wpt_register_js' );



function wpt_register_css() {
    wp_register_style( 'bootstrap.min', get_template_directory_uri() . '/css/bootstrap.min.css' );
    wp_enqueue_style( 'bootstrap.min' );
}
add_action( 'wp_enqueue_scripts', 'wpt_register_css' );
add_theme_support( 'post-thumbnails' );

?>

<?php // Register custom navigation walker
require_once('wp_bootstrap_navwalker.php');

?>


<!-- PORTFOLIO CUSTOM POST -->
<?php

function my_custom_post_portfolio() {
$labels = array(
    'name'               => _x( 'Portfolio', 'post type general name' ),
    'singular_name'      => _x( 'Portfolio-item', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New Product' ),
    'edit_item'          => __( 'Edit Product' ),
    'new_item'           => __( 'New Product' ),
    'all_items'          => __( 'All Products' ),
    'view_item'          => __( 'View Product' ),
    'search_items'       => __( 'Search Products' ),
    'not_found'          => __( 'No products found' ),
    'not_found_in_trash' => __( 'No products found in the Trash' ),
    'parent_item_colon'  => '',
    'menu_name'          => 'Portfolio'
);
  $args = array(
      'labels'        => $labels,
      'description'   => 'Holds our products and product specific data',
      'public'        => true,
      'menu_position' => 5,
      'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
      'has_archive'   => true,
  );
  register_post_type( 'portfolio', $args );
}
add_action( 'init', 'my_custom_post_portfolio' );


?>
<!-- BLOG CUSTOM POST -->
<?php

function my_custom_post_blog() {
    $labels = array(
        'name'               => _x( 'Blog', 'post type general name' ),
        'singular_name'      => _x( 'Blog-item', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'book' ),
        'add_new_item'       => __( 'Add New Product' ),
        'edit_item'          => __( 'Edit Product' ),
        'new_item'           => __( 'New Product' ),
        'all_items'          => __( 'All Products' ),
        'view_item'          => __( 'View Product' ),
        'search_items'       => __( 'Search Products' ),
        'not_found'          => __( 'No products found' ),
        'not_found_in_trash' => __( 'No products found in the Trash' ),
        'parent_item_colon'  => '',
        'menu_name'          => 'Blogs'
    );
    $args = array(
        'labels'        => $labels,
        'description'   => 'Holds our products and product specific data',
        'public'        => true,
        'menu_position' => 5,
        'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
        'has_archive'   => true,
    );
    register_post_type( 'Blog', $args );
}
add_action( 'init', 'my_custom_post_blog' );


?>

<!-- ABOUT CUSTOM POST -->
<?php

function my_custom_post_about() {
    $labels = array(
        'name'               => _x( 'About', 'post type general name' ),
        'singular_name'      => _x( 'About-item', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'book' ),
        'add_new_item'       => __( 'Add New Product' ),
        'edit_item'          => __( 'Edit Product' ),
        'new_item'           => __( 'New Product' ),
        'all_items'          => __( 'All Products' ),
        'view_item'          => __( 'View Product' ),
        'search_items'       => __( 'Search Products' ),
        'not_found'          => __( 'No products found' ),
        'not_found_in_trash' => __( 'No products found in the Trash' ),
        'parent_item_colon'  => '',
        'menu_name'          => 'About'
    );
    $args = array(
        'labels'        => $labels,
        'description'   => 'Holds our products and product specific data',
        'public'        => true,
        'menu_position' => 5,
        'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
        'has_archive'   => true,
    );
    register_post_type( 'About', $args );
}
add_action( 'init', 'my_custom_post_about' );


?>



<?php
/**
 * Register sidebar
 *
 */
function wpb_widgets_init() {

    register_sidebar( array(
        'name' => __( 'vimeo', 'wpb' ),
        'id' => 'sidebar-1',
        'description' => __( 'The main sidebar appears at the bottom of each page ', 'wpb' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s col-lg-12"><ul class="row"><li>',
        'after_widget' => '</li></ul></section>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );

    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'wpb' ),
        'id' => 'sidebar-1',
        'description' => __( 'The main sidebar appears at the bottom of each page ', 'wpb' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s col-lg-3"><ul class="row"><li>',
        'after_widget' => '</li></ul></section>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );

}

add_action( 'widgets_init', 'wpb_widgets_init' );
