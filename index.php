<?php
    get_header();
?>






    <ul id="header-wrapper" class="rslides wrapper" role="header">

        <li class="slide slide1 text-center ">
            <section class="container">
                <header class="row">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/styles/images/Logo.svg" alt="" class="img-responsive col-lg-offset-5 col-lg-2" />

                </header>
                <article class="row text-center white">

                    <h1>PORTFOLIO WEBSITE</h1>


                    <p><em>&copy; In opdracht van Arteveldehogeschool 2013-2014"</em></p>

                </article>
                <footer class="row text-center">
                    <button data-scroll href="#contact" type="button" class="buttonwhite"><span class="black">CONTACT ME!</span></button>
                </footer>



            </section>

        </li>

    </ul>

    <div class="wrapper">

    </div>


    <div id="pre-content-wrapper" class="wrapper darkcarbonbg" style="opacity: 0.9;">
        <div class="container">
            <header class="row text-center white">
                <h1>Hello my name is <span class="beige">Matthias Mandiau</span></h1>
            </header>
            <section class="row text-center">
                <p class="grey">This is a  website about my own personal <span class="beige">Portfolio.</span> Here you can find all sorts of information<br> regarding personal information about  <span class="beige">Me or my work.</span></p>
                <p><em class="beige">"Lorem ipsum Dolores"</em></p>
            </section>
        </div>
    </div>






    <div id="content-header-wrapper" class="wrapper whitebg">
        <section class="container">
            <header class="row text-center">
                <img src="<?php bloginfo('stylesheet_directory'); ?>/styles/images/mustache.png" alt="" class="img-responsive" style="margin: 0 auto"; />
                <h1>PHILOSOPHY</h1>
                <p>I would like to introduce myself with the living idea of how I see my future.<br>
                I try to keep my mind resting on 2 main things</p>
                <hr>
                <hr>


            </header>
            <section class="row">
                <article class="col-lg-6">
                    <ul class="tarieventabel text-center">
                        <li class="fa fa-5x fa-globe icon"></li>
                        <li class="title">THINK BIG</li>
                        <li class="description">Nullam vulputate nibh at nisi, nec nunc iaculis. Nulla porttitor neque vitae ligula luctus imperdiet molestie at dui.</li>





                    </ul>
                </article>
                <article class="col-lg-6">
                    <ul class="tarieventabel text-center">
                        <li class="fa fa-5x fa-lightbulb-o icon"></li>
                        <li class="title">START SMALL</li>
                        <li class="description">Nullam vulputate nibh at
                            Nullam vulputate nibh at.<br>
                            Nullam vulputate nibh at.</li>






                    </ul>
                </article>

            </section>
            <footer class="text-center row">
                <a data-scroll href="#services" class="downbutton" ><img src="<?php bloginfo('stylesheet_directory'); ?>/styles/images/down.svg" alt="" style="opacity: 0.9;" /></a>
            </footer>


        </section>



    </div>
    <div id="services" class="wrapper darkcarbonbg" style="opacity: 0.9;">
        <section class="container">
            <header class="row text-center">
                <img src="<?php bloginfo('stylesheet_directory'); ?>/styles/images/crosses.png" alt="" class="img-responsive" style="margin: 0 auto"; />
                <h1 class="grey">SERVICES</h1>

                <p class="grey">Here you will find an overview of which services I provide</p>
                <hr>
                <hr>
            </header>

            <section id="tarieventabellen" class="row grey">

                <ul class="tarieventabel col-lg-4 text-center">
                    <li class="fa fa-5x fa-pencil-square-o icon"></li>
                    <li class="title">DESIGN</li>
                    <li class="description">Design often necessitates considering the aesthetic and functional dimensions of both the design object and process. It involves research, thought, modeling, interactive adjustment, and re-design. </li>

                    <!-- <li class="price">€8</li> -->




                </ul>

                <ul class="tarieventabel col-lg-4  text-center">
                    <li class=" fa fa-5x fa-keyboard-o icon"></li>
                    <li class="title">CODE</li>
                    <li class="description">Coding often necessitates considering the aesthetic and functional dimensions of both the design object and process. It involves research, thought, modeling, interactive adjustment, and re-design. </li>

                    <!--   <li class="price">€85</li>-->
                    <!--    <li class="price">€95</li>-->


                </ul>

                <ul class="tarieventabel col-lg-4  text-center">
                    <li class="fa fa-5x fa-camera-retro icon"></li>
                    <li class="title">PHOTOGRAPHY</li>
                    <li class="description">Photography often necessitates considering the aesthetic and functional dimensions of both the design object and process. It involves research, thought, modeling, interactive adjustment, and re-design.</li>
                    <!--  <li class="price">€8</li>-->

                </ul>

            </section>
    </div>





    <div id="portfolio" class="wrapper carbonbg">
        <div class="container">
            <section class="container">
                <header class="row text-center">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/styles/images/mustache.png" alt="" class="img-responsive" style="margin: 0 auto"; />
                    <h1>PORTFOLIO</h1>
                    <p>Nullam vulputate nibh at nisi, nec nunc iaculis. Nulla porttitor neque<br>
                        Kijk gerust maar eens rond.</p>
                    <hr>
                    <hr>

                </header>


                <section id="portfolio-grid" class="row">



                    <?php



                    query_posts( 'post_type=portfolio' );

                    while ( have_posts() ) : the_post();

                        if ( has_post_thumbnail() ) {
                            echo '<section class="col-lg-4 col-md-4" style="padding: 0 16px 30px 0;">';



                             if (has_post_thumbnail( $post->ID ) ): ?>
                                <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                                 <div class="img">
                                 <div id="portfolio-item" style="background: url('<?php echo $image[0]; ?>') center center; background-size:cover;"></div>


                                     <div class="overlay">
                                         <a href="<?php echo the_permalink(); ?>" class="expand text-center"><?php the_title(); ?></a>
                                         <a class="close-overlay hidden">x</a>
                                     </div>
                                 </div>

                            <?php endif;


                            //echo '<section class="img-responsive thumbnail">';
                           // the_post_thumbnail();
                           // echo'</section>';
                            echo '</section>';
                        }
                        else
                        {
                            echo '<section class="col-lg-4"">';
                            echo '<h3>';
                            echo '<a href="';
                            the_permalink();
                            echo '" alt="">';
                            the_title();
                            echo '</a>';
                            echo '</h3>';
                            echo '<p>';
                            the_excerpt();
                            echo '</p>';
                            echo'</section>';
                        }




                     endwhile;
                    if( ! have_posts() ) : ?>
                        <article class="text-center col-lg-12">
                            <h1>** NO PORTFOLIOITEMS FOUND **</h1>
                        </article>
                    <?php endif;
                    // Reset Query
                    wp_reset_query();

                    ?>
                </section>



        </div>
    </div>



    <div id="tip-wrapper" class="wrapper darkcarbonbg" style="opacity: 0.9;">
        <div class="container">

            <header class="row text-center">
                <img src="<?php bloginfo('stylesheet_directory'); ?>/styles/images/crosses.png" alt="" class="img-responsive" style="margin: 0 auto"; />
                <h1 class="grey col-lg-12">VIMEO</h1>
                <p class="grey">Here you will find my latest video that I made for school.</p>
                <hr>
                <hr>


            </header>
            <section class="row">
            <section class="col-lg-6 text-center">
                <h2 class="grey">Report (Short version)</h2>
                <?php echo do_shortcode('[fve]https://vimeo.com/95449727[/fve]'); ?>
            </section>
            <section class="col-lg-6 text-center">
                    <h2 class="grey">Report (Longer version)</h2>
                    <?php echo do_shortcode('[fve]https://vimeo.com/94505035[/fve]'); ?>
            </section>




            </section>

        </div>
    </div>




    <div id="about" class="wrapper">
        <div class="container">
            <header class="row text-center">
                <img src="<?php bloginfo('stylesheet_directory'); ?>/styles/images/mustache.png" alt="" class="img-responsive" style="margin: 0 auto"; />
                <h1>ABOUT</h1>
                <p>Some more information about myself.</p>
                <hr>
                <hr>
            </header>
            <section class="row">
                <figure class="col-lg-4">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/styles/images/about.jpg" alt="" class="img-responsive" />
                </figure>
                <article class="col-lg-8">
                    <h3>My name: Matthias Mandiau</h3>
                    <p>In tegenstelling tot wat algemeen aangenomen wordt is Lorem Ipsum niet zomaar willekeurige tekst. het heeft zijn wortels in een stuk klassieke latijnse literatuur uit 45 v.Chr. en is dus meer dan 2000 jaar oud. Richard McClintock, een professor latijn aan de Hampden-Sydney College in Virginia, heeft één van de meer obscure latijnse woorden, consectetur, uit een Lorem Ipsum passage opgezocht, en heeft tijdens het zoeken naar het woord in de klassieke literatuur de onverdachte bron ontdekt. Lorem Ipsum komt uit de secties 1.10.32 en 1.10.33 van "de Finibus Bonorum et Malorum" (De uitersten van goed en kwaad) door Cicero, geschreven in 45 v.Chr. Dit boek is een verhandeling over de theorie der ethiek, erg populair tijdens de renaissance. De eerste regel van Lorem Ipsum, "Lorem ipsum dolor sit amet..", komt uit een zin in sectie 1.10.32.</p>
                </article>
            </section>

        </div>

    </div>
    <div  id="social-media-wrapper" class="wrapper">
        <a href="http://facebook.com/matthiasmandiau" id="facebook" class="col-lg-4 socialmedia text-center">
            <p class="fa fa-5x fa-facebook socialicon"></p>
        </a>
        <a href="https://vimeo.com/user21459529/videos" id="vimeo" class="col-lg-4 socialmedia  text-center">
            <p class="fa fa-5x fa-vimeo-square socialicon"></p>
        </a>
        <a href="http://twitter.com/matthiasmandiau" id="twitter" class="col-lg-4 socialmedia  text-center">
            <p class="fa fa-5x fa-twitter socialicon"></p>
        </a>

    </div>

    <div id="blog" class="wrapper carbonbg">
        <section class="container">
            <header class="row text-center">
                <h1>BLOG</h1>

                <p>Hier vindt u een overzicht terug van alle blogberichten.</p>
            </header>

            <section class="row text-center">

                <ul>
                    <?php

                    // The Query
                    query_posts( 'post_type=blog' );

                    // The Loop
                    while ( have_posts() ) : the_post(); ?>
                        <li class="col-lg-4"><h3><a href="<?php the_permalink() ?>" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3><p class="date"> <?php the_time("j M Y"); ?></p><p><?php the_excerpt(); ?></p></li>

                    <?php endwhile;
                    if( ! have_posts() ) : ?>
                        <article class="text-center col-lg-12">
                            <h1>** NO BLOGITEMS FOUND **</h1>
                        </article>
                    <?php endif;
                    // Reset Query
                    wp_reset_query();

                    ?>
                </ul>





            </section>

        </section>
    </div>





    <div id="contact" class="wrapper darkcarbonbg" style="opacity: 0.9;">
        <div class="container">
            <header class="row text-center white">
                <img src="<?php bloginfo('stylesheet_directory'); ?>/styles/images/crosses.png" alt="" class="img-responsive" style="margin: 0 auto"; />
                <h1>CONTACT</h1>
                <p>Contacteer ons via onderstaand formulier voor enige vragen of reservaties.</p>
                <hr>
                <hr>
            </header>




            <div class="row">




                <section class="col-lg-12">
                    <?php echo do_shortcode('[contact-form-7 id="4" title="Contactformulier 1"]'); ?>


                </section>



            </div>





        </div>


    </div>
    <div id="map-canvas" class="wrapper"></div>




<?php get_sidebar();?>

<?php get_footer(); ?>