
</div>


  <div id="footer" class="wrapper darkcarbonbg" role="footer">
      <div class="container">
          <span class="scrolltotop">Scroll To Top</span>
          <article>
              <p class="row text-center footertext white">&copy; 2013-2014 <a href="#header-wrapper">ARTEVELDEHOGESCHOOL</a>, ALL RIGHTS RESERVED.</p>
          </article>

    </div>
</div>


<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/scripts/vendor/jquery.js"></script>

<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/scripts/vendor/fastclick.js"></script>

<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/scripts/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/scripts/bootstrap/transition.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/scripts/bootstrap/collapse.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/scripts/jquery.swipebox.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/scripts/responsiveslides.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/scripts/smooth-scroll.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/scripts/jquery.sudoSlider.min.js"></script>

<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/https://maps.googleapis.com/maps/api/js?key=AIzaSyCWBWMDKDMfMDHPiz8r_W9GSNxxSOm3TI8&sensor=true"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/scripts/jquery.sticky.js"></script>
<script>

    (function(){
        $("#nav-wrapper").sticky({ topSpacing: 0 });

    })();


</script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/scripts/global.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/scripts/jquery.vscroll.js"></script>

<script type="text/javascript">
    (function(){
        $('.slide').VScroll(2);

    })();
</script>
<script type="text/javascript">
    smoothScroll.init();
</script>

<script type="text/javascript">


    function initialize() {
        var myLatlng = new google.maps.LatLng(51.046303, 3.726254);
        var mapOptions = {
            zoom: 10,
            center: myLatlng,
            styles: [{"featureType":"water","stylers":[{"visibility":"on"},{"color":"#acbcc9"}]},{"featureType":"landscape","stylers":[{"color":"#f2e5d4"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#c5c6c6"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#e4d7c6"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#fbfaf7"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#c5dac6"}]},{"featureType":"administrative","stylers":[{"visibility":"on"},{"lightness":33}]},{"featureType":"road"},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":20}]},{},{"featureType":"road","stylers":[{"lightness":20}]}]

        }
        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'Hello World!'
        });
    }

    function loadScript() {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&' +
            'callback=initialize';
        document.body.appendChild(script);
    }

    window.onload = loadScript;</script>
</script>







<?php wp_footer(); ?>
  </body>
</html>
