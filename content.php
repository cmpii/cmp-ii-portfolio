<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


	<header class="entry-header">

		<?php
			if ( is_single() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h1 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' );
			endif;
		?>


	</header><!-- .entry-header -->


	<div class="row">
		<?php

        if ( has_post_thumbnail() ) {
            echo '<section class="col-lg-4">';
            echo '<div class="img-responsive thumbnail">';
            the_post_thumbnail();
            echo'</section>';
            echo '<section class="col-lg-8">';

            echo '<p>';
            the_excerpt();
            echo '</p>';
            echo'</section>';
        }
        else
        {
            echo '<section class="col-lg-8">';
            echo '<p>';
            the_excerpt();
            echo '</p>';
            echo'</section>';
        }





		?>
	</div><!-- .entry-content -->


	<?php the_tags( '<footer class="entry-meta"><span class="tag-links">', '', '</span></footer>' ); ?>
</article><!-- #post-## -->
